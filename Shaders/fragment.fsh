#version 330 

in vec3 normal;
in vec3 frag_pos;
in vec3 cam_pos;

out vec4 col;
void main()
{
    vec3 light_normal = normalize( cam_pos - frag_pos);
    float diff = dot(light_normal, normalize(normal));
    float x = ((10.0-diff)/10.0);
    col = vec4(1.0 * x, 0.75 * x, 0.5 * x, 1.0);
}