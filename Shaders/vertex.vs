#version 330 

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNorm;

uniform mat4 p_matrix;
uniform mat4 v_matrix;
uniform mat4 m_matrix;

uniform vec3 camera_pos;

out vec3 normal;
out vec3 frag_pos;
out vec3 cam_pos;

void main()
{
    normal = aNorm;
    vec4 final_vertex = p_matrix * v_matrix * m_matrix * vec4(aPos.x, aPos.y, aPos.z, 1.0);

    frag_pos = vec3(m_matrix * vec4(aPos, 1.0));
    cam_pos = camera_pos;

    gl_Position = final_vertex; 
}