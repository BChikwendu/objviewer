#pragma once

#include <fstream>
#include <iostream>
#include <fstream>

#include <sstream>
#include <string>

#include <vector>
#include <queue>


#include "Mesh.h"

class OBJparser
{
public:
	OBJparser(std::string file);
	std::vector<Vertex> ParseFile();



private:
	std::vector<std::string> file_lines;



};

