#include "Camera.h"

Camera::Camera()
{
	position = glm::vec3(0.0f, 0.0f, -3.0f);
	
	model_matrix = glm::mat4(1.0f);
	model_matrix = glm::scale(model_matrix, glm::vec3(1.0f,1.0f,1.0f));

	view_matrix = glm::mat4(1.0f);
	view_matrix = glm::translate(view_matrix, glm::vec3(position.y, position.x, position.z));

	projection_matrix = glm::perspective(glm::radians(35.0f), 800.0f / 600.0f, 0.1f, 100.0f);
}

glm::mat4 Camera::GetFinalTransform()
{
	view_matrix = glm::translate(view_matrix, glm::vec3(position.y, position.x, position.z));
	view_matrix = glm::lookAt(position, glm::vec3(0.0, 0.0, 0.0), glm::vec3(0.0, 1.0, 0.0));
	return projection_matrix * view_matrix * model_matrix;
}

glm::mat4 Camera::GetProjection()
{
	return projection_matrix;
}

glm::mat4 Camera::GetView()
{
	view_matrix = glm::translate(view_matrix, glm::vec3(position.y, position.x, position.z));
	view_matrix = glm::lookAt(position, glm::vec3(0.0, 0.0, 0.0), glm::vec3(0.0, 1.0, 0.0));
	return view_matrix;
}

glm::mat4 Camera::GetModel()
{
	return model_matrix;
}

glm::vec3 Camera::GetPosition()
{
	return glm::vec3(position.y, position.x, position.z);
}

void Camera::Translate(float x, float y, float z)
{
	position.x += x;
	position.y += y;
	position.z += z;
}
