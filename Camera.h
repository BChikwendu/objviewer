#pragma once
#include <glm.hpp>
#include <ext.hpp>

/// <summary>
/// Handles the linear algebra stuff
/// </summary>
class Camera
{
public:
	Camera();
	glm::mat4 GetFinalTransform();
	glm::mat4 GetProjection();
	glm::mat4 GetView();
	glm::mat4 GetModel();
	glm::vec3 GetPosition();
	void Translate(float x, float y, float z);

private:
	glm::vec3 position;
	

	glm::mat4 model_matrix;
	glm::mat4 view_matrix;
	glm::mat4 projection_matrix;
};

