#include "Mesh.h"

Mesh::Mesh()
{
     float test_data[] = {
  -1.0f, -1.0f, 0.0f,
  1.0f, -1.0f, 0.0f,
  0.0f,  1.0f, 0.0f,
    };

     Vertex v1; v1.position[0] = test_data[0]; v1.position[1] = test_data[1]; v1.position[2] = test_data[2];
     Vertex v2; v2.position[0] = test_data[3]; v2.position[1] = test_data[4]; v2.position[2] = test_data[5];
     Vertex v3; v3.position[0] = test_data[6]; v3.position[1] = test_data[7]; v3.position[2] = test_data[8];

     v1.normal[0] = 0.0;  v1.normal[1] = 0.0; v1.normal[0] = 1.0;
     v2.normal[0] = 0.0;  v2.normal[1] = 0.0; v2.normal[0] = 1.0;
     v3.normal[0] = 0.0;  v3.normal[1] = 0.0; v3.normal[0] = 1.0;

     vertex_array.push_back(v1);
     vertex_array.push_back(v2);
     vertex_array.push_back(v3);
}

Vertex* Mesh::GetData()
{
    return &vertex_array[0];
}

int Mesh::GetVertexArraySize()
{
    return vertex_array.size() * sizeof(Vertex);
}

void Mesh::SetVertexArray(std::vector<Vertex> data)
{
    vertex_array.clear();
    for (auto var : data) {
        vertex_array.push_back(var);
    }
}
