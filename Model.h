#pragma once
#include "Mesh.h"
#include <glad.h>
#include <GLFW/glfw3.h>

class Model
{
public:
    Model();
    Model(Mesh m);
    void Draw();

//private:
    GLuint vertex_array_object;
    GLuint vertex_buffer;
    Mesh mesh;
    

};

