#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include <optional>

#define GLFW_INCLUDE_NONE
#include <glad.h>
#include <GLFW/glfw3.h>


enum class ShaderType
{
	Vertex,
	Fragment
};

class Shader
{
public:
	Shader(ShaderType type);
	Shader(ShaderType type, std::string file_path);
	~Shader();
	void AttachToProgram(int shader_program);
	ShaderType type_;

private:
	std::string	shader_string_;
	int shader_object_id_;
	std::optional<std::string> CheckSuccess();

	
};

