#include "Model.h"

Model::Model()
{
	glGenVertexArrays(1, &vertex_array_object);
	glBindVertexArray(vertex_array_object);

	//Bind vertex buffers, UV buffers etc here. Grab data from Mesh
	glGenBuffers(1, &vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);

	
	Vertex* data_pointer = mesh.GetData();
	int size = mesh.GetVertexArraySize();

	glBufferData(GL_ARRAY_BUFFER,
				size,
				data_pointer,
				GL_STATIC_DRAW);


	//Vertex Position data
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
	    sizeof(Vertex),                  // stride (0 let's opengl figure it out)
		(void*)0            // array buffer offset
	);

	//Normal Position Data
	
	glVertexAttribPointer(
		1,                  // attribute 1
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		sizeof(Vertex),                // stride (0 let's opengl figure it out)
		(void*)offsetof(Vertex,Vertex::normal)    // array buffer offset
	);
	
}

Model::Model(Mesh m)
{
	mesh = m;
	glGenVertexArrays(1, &vertex_array_object);
	glBindVertexArray(vertex_array_object);

	//Bind vertex buffers, UV buffers etc here. Grab data from Mesh
	glGenBuffers(1, &vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);


	Vertex* data_pointer = mesh.GetData();
	int size = mesh.GetVertexArraySize();

	glBufferData(GL_ARRAY_BUFFER,
		size,
		data_pointer,
		GL_STATIC_DRAW);


	//Vertex Position data
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		sizeof(Vertex),                  // stride (0 let's opengl figure it out)
		(void*)0            // array buffer offset
	);

	//Normal Position Data

	glVertexAttribPointer(
		1,                  // attribute 1
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		sizeof(Vertex),                // stride (0 let's opengl figure it out)
		(void*)offsetof(Vertex, Vertex::normal)    // array buffer offset
	);

}

void Model::Draw()
{
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glDrawArrays(GL_TRIANGLES, 0, (mesh.GetVertexArraySize() / sizeof(Vertex)));

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}
