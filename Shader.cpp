#include "Shader.h"

Shader::Shader(ShaderType type)
{
    if (type == ShaderType::Vertex) {
        shader_string_ = "#version 330 core\n"
            "layout (location = 0) in vec3 aPos;\n"
            "void main()\n"
            "{\n"
            "   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
            "}\0";

        shader_object_id_ = glCreateShader(GL_VERTEX_SHADER);
        auto string_source = shader_string_.c_str();
        glShaderSource(shader_object_id_, 1, &string_source, NULL);
        glCompileShader(shader_object_id_);

        std::cout << "Vertex shader result: " << CheckSuccess().value_or("Success!\n");
     
    }
    else {
        shader_string_ = "#version 330 core\n"
            "out vec4 FragColor;\n"
        "void main()\n"
        "{\n"
            "FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
       " } \n";

        shader_object_id_ = glCreateShader(GL_FRAGMENT_SHADER);
        auto string_source = shader_string_.c_str();
        glShaderSource(shader_object_id_, 1, &string_source, NULL);
        glCompileShader(shader_object_id_);

        std::cout << "Fragment shader result: " << CheckSuccess().value_or("Success!\n");
    
    }
    
}

Shader::Shader(ShaderType type, std::string file_path)
{
    if (type == ShaderType::Vertex) {

        char errorbuffer[256];
        std::ifstream stream(file_path);
        if (stream.fail()) {
            strerror_s(errorbuffer, errno);
            std::cout << "Stream failed: " << errorbuffer;
        }

       
        std::string shader_line;
        while (std::getline(stream, shader_line)) {
            shader_string_.append((shader_line + "\n"));
        }

        shader_object_id_ = glCreateShader(GL_VERTEX_SHADER);
        auto string_source = shader_string_.c_str();
        glShaderSource(shader_object_id_, 1, &string_source, NULL);
        glCompileShader(shader_object_id_);

        std::cout << "Vertex shader result: " << CheckSuccess().value_or("Success!\n");
        stream.close();
    }
    else {
        std::ifstream stream(file_path);
       
        char errorbuffer[256];
        if (stream.fail()) {
            strerror_s(errorbuffer, errno);
            std::cout << "Stream failed: " << errorbuffer;
        }
        
        std::string shader_line;
        while (std::getline(stream, shader_line)) {
            shader_string_.append((shader_line + "\n"));
        }


        shader_object_id_ = glCreateShader(GL_FRAGMENT_SHADER);
        auto string_source = shader_string_.c_str();
        glShaderSource(shader_object_id_, 1, &string_source, NULL);
        glCompileShader(shader_object_id_);

        std::cout << "Fragment shader result: " << CheckSuccess().value_or("Success!\n");
        stream.close();
    }
  
}

Shader::~Shader()
{
    glDeleteShader(shader_object_id_);
}

void Shader::AttachToProgram(int shader_program)
{
    glAttachShader(shader_program, shader_object_id_);
}

std::optional<std::string> Shader::CheckSuccess()
{
    int  success;
    char infoLog[512];
    glGetShaderiv(shader_object_id_, GL_COMPILE_STATUS, &success);

    if (!success)
    {
        glGetShaderInfoLog(shader_object_id_, 512, NULL, infoLog);
        return infoLog;
    }
    else
        return std::nullopt;
    
}
