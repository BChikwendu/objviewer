#include "OBJparser.h"


OBJparser::OBJparser(std::string file)
{
	std::ifstream stream(file);

    char errorbuffer[256];
    if (stream.fail()) {
        strerror_s(errorbuffer, errno);
        std::cout << "Stream failed: " << errorbuffer;
    }
    
    std::string curr_line;
    while (std::getline(stream,curr_line)) {
        if (!(curr_line[0] == '#')) {
            file_lines.push_back(curr_line);
        }    
       
    }

}

std::vector<Vertex> OBJparser::ParseFile()
{
    
    std::vector<Vertex> v_array;
    std::vector<Vertex> normal_array;
    
    std::vector<Vertex> final_vertices;

    //Fill arrays of the different attributes, v, vn, 
    for (auto var : file_lines) {
        if (var[0] == 'v' && var[1] != 'n' && var[1] != 't') {
            //Describes a vertex position
            std::istringstream line(var);
            std::vector<std::string> line_tokens;
            
            std::string token;
            while (std::getline(line, token, ' ')) {
                if (token.find_first_not_of(' ') != std::string::npos && token != "v") {
                    line_tokens.push_back(token);
         
                }
            
            }
             
        
            
            Vertex vy; 
            vy.position[0] = std::atof(line_tokens[0].c_str()); vy.position[1] = std::atof(line_tokens[1].c_str()); vy.position[2] = std::atof(line_tokens[2].c_str());
            v_array.push_back(vy);
        }

        if (var[0] == 'v' && var[1] == 'n') {
            //Normal 
            std::istringstream line(var);
            std::vector<std::string> line_tokens;

            std::string token;
            while (std::getline(line, token, ' ')) {
                if (token.find_first_not_of(' ') != std::string::npos && token != "vn") {
                    line_tokens.push_back(token);

                }

            }


            
            Vertex vy;
            vy.normal[0] = std::atof(line_tokens[0].c_str()); vy.normal[1] = std::atof(line_tokens[1].c_str()); vy.normal[2] = std::atof(line_tokens[2].c_str());
            normal_array.push_back(vy);
        }

        if (var[0] == 'v' && var[1] == 't') {
            //Texture variable (We don't store these yet)
        }

        if (var[0] == 'f') {
            //Construct the three vertices that make up the face

            std::istringstream line(var);
            std::queue<std::string> line_tokens;

            std::string token;
            while (std::getline(line, token, ' ')) {
                if (token.find_first_not_of(' ') != std::string::npos && token != "f") {
                    line_tokens.push(token);
                }

            }

            //line_tokens has each v/vt/vn, take each group and generate a vertex
            for (int i = 0; i < 3; i++) {
                std::string group_token = line_tokens.front();
                line_tokens.pop();
                int token_length = group_token.length();

                const char* v_char = &group_token[0];
                const char* vn_char = &group_token[token_length - 1];

                Vertex v1;
                v1.position[0] = v_array[std::atoi(v_char) - 1].position[0];
                v1.position[1] = v_array[std::atoi(v_char) - 1].position[1];
                v1.position[2] = v_array[std::atoi(v_char) - 1].position[2];

                v1.normal[0] = normal_array[std::atoi(vn_char) - 1].normal[0];
                v1.normal[1] = normal_array[std::atoi(vn_char) - 1].normal[1];
                v1.normal[2] = normal_array[std::atoi(vn_char) - 1].normal[2];
                final_vertices.push_back(v1);

            }
           

            
        }
    }

    return final_vertices;
}
