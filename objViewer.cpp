// objViewer.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

#define GLFW_INCLUDE_NONE
#include <glad.h>
#include <GLFW/glfw3.h>


#include "Camera.h"
#include "Shader.h"
#include "Model.h"
#include "OBJparser.h"

void error_callback(int error, const char* description)
{
    std::cout << "Error: " << description;
}
void resize_callback(GLFWwindow* window, int window_width, int window_height) {
    glViewport(0, 0, window_width, window_height);
    
}
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);

Camera camera;

int main()
{
    const int kWindowHeight = 640;
    const int kWindowWidth = 640;
    if (!glfwInit())
    {
        // Initialization failed
        std::cout << "Failed to initialize GLFW\n";
    }
    
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    GLFWwindow* window = glfwCreateWindow(kWindowWidth, kWindowHeight, "OBJ renderer", NULL, NULL);

    if (!window)
    {
        std::cout << "Failed to create window\n";
    }

    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        std::cout << "Failed to initialize GLAD\n";
    }

    //Set callbacks
    glfwSetErrorCallback(error_callback);
    glfwSetWindowSizeCallback(window, resize_callback);
    glfwSetKeyCallback(window, key_callback);

    Shader vertexShader(ShaderType::Vertex,"Shaders/vertex.vs");
    Shader fragmentShader(ShaderType::Fragment,"Shaders/fragment.fsh");


    unsigned int shaderProgram;
    shaderProgram = glCreateProgram();

    vertexShader.AttachToProgram(shaderProgram);
    fragmentShader.AttachToProgram(shaderProgram);
    glLinkProgram(shaderProgram);

    int success;
    char infoLog[256];
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cout <<"\n Program failed: " << infoLog;
    }

    glUseProgram(shaderProgram);

    glEnable(GL_CULL_FACE);

    OBJparser parser("Models/teapot.obj");

    Mesh box_mesh;
    box_mesh.SetVertexArray(parser.ParseFile());
    
    Model triangle(box_mesh);

    int p_matrix_location = glGetUniformLocation(shaderProgram, "p_matrix");
    int v_matrix_location = glGetUniformLocation(shaderProgram, "v_matrix");
    int m_matrix_location = glGetUniformLocation(shaderProgram, "m_matrix");

    int cam_pos_location = glGetUniformLocation(shaderProgram, "camera_pos");

    glViewport(0, 0, kWindowWidth, kWindowHeight);

    while (!glfwWindowShouldClose(window))
    {

        glClear(GL_COLOR_BUFFER_BIT);
      
        triangle.Draw();
        
        auto p_data = camera.GetProjection();
        auto v_data = camera.GetView();
        auto m_data = camera.GetModel();
        auto cam_pos_data = camera.GetPosition();

        glUniformMatrix4fv(p_matrix_location, 1, false, &p_data[0][0]);
        glUniformMatrix4fv(v_matrix_location, 1, false, &v_data[0][0]);
        glUniformMatrix4fv(m_matrix_location, 1, false, &m_data[0][0]);
        glUniform3fv(cam_pos_location, 1, &cam_pos_data[0]);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

   
    

    glfwDestroyWindow(window);
    glfwTerminate();
    return 0;
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_D ) {
        camera.Translate(0.2, 0, 0);
    }
    if (key == GLFW_KEY_A) {
        camera.Translate(-0.2, 0, 0);
    }
    if (key == GLFW_KEY_W) {
        camera.Translate(0, 0, 0.2);
    }
    if (key == GLFW_KEY_S) {
        camera.Translate(0, 0, -0.2);
    }
    if (key == GLFW_KEY_Q) {
        camera.Translate(0, -0.2, 0.0);
    }
    if (key == GLFW_KEY_E) {
        camera.Translate(0, 0.2, 0.0);
    }
}