#pragma once
#include <vector>
/// <summary>
/// A class that can encapsulate an array of GLFloats. And generate one from many different sources.
/// </summary>

struct Vertex
{
	float position[3];
	float normal[3];
};
class Mesh
{
public:
	Mesh();
	Vertex* GetData();
	int GetVertexArraySize();
	void SetVertexArray(std::vector<Vertex> data);
private:
	std::vector<Vertex> vertex_array;
};

